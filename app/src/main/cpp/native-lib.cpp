//
// Created by adrika on 27/12/19.
//

#include <jni.h>
#include <string>
#include <string.h>


extern "C" JNIEXPORT jstring JNICALL
Java_id_ac_ui_cs_mobileprogramming_adrikanovrialdi_sinonimantonimbahasaindonesia_service_RetrofitService_getAPIUrl (
        JNIEnv* env, jobject) {
    std::string apiUrl = "https://grammar-service.herokuapp.com/api/grammar/id/";

    return env->NewStringUTF(apiUrl.c_str());
}