package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Timestamp;
import java.util.Date;

@Entity(tableName = "words_entity")
public class WordsEntity {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "words")
    private String words;

    @ColumnInfo(name = "count_search")
    private long countSearch;

    @ColumnInfo(name = "timestamp")
    private long timestamp;

    @NonNull
    public String getWords() {
        return words;
    }

    public void setWords(@NonNull String words) {
        this.words = words;
    }

    public long getCountSearch() {
        return countSearch;
    }

    public void setCountSearch(long countSearch) {
        this.countSearch = countSearch;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void createOrUpdateTimestamp() {
       this.timestamp = new Date().getTime();
    }
}
