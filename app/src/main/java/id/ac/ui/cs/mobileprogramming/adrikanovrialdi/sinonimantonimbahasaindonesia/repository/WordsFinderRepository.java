package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.repository;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.dao.WordsEntityDao;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.database.WordEntityDatabase;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.factory.WordsEntityFactory;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsResponse;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.service.GrammarAPI;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.service.RetrofitService;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.service.WordsResponseCallBack;

public class WordsFinderRepository {
    private static WordsFinderRepository wordsFinderRepository = new WordsFinderRepository();
    private boolean isInitialize = false;
    private GrammarAPI grammarAPI;
    private WordsEntityDao wordsEntityDao;
    private static MutableLiveData<List<WordsEntity>> lastestWordsData = new MutableLiveData<>();


    public static WordsFinderRepository getInstance() {
        return wordsFinderRepository;
    }

    public void setApplicationContext(Context context) {
        WordEntityDatabase database = WordEntityDatabase.getDatabase(context);
        wordsEntityDao = database.getWordsEntityDao();
    }

    private void insertIntoDatabase(WordsEntity wordsEntity) {
        new DatabaseInsertAsyncTaskHelper(wordsEntityDao).execute(wordsEntity);
        findAllWordsData();
    }

    private void updateDatabaseData(WordsEntity wordsEntity) {
        new DatabaseUpdateAsyncTaskHelper(wordsEntityDao).execute(wordsEntity);
        findAllWordsData();
    }

   private void findWordsInDatabase(String words) throws InterruptedException, ExecutionException {
       WordsEntity wordsEntity = new DatabaseFindAsyncTaskHelper(wordsEntityDao, words).execute().get();
        if (wordsEntity!=null) {
            System.out.println("Found " + words+ " in the database");
            System.out.println("Trying to update data..");
            updateDatabaseData(wordsEntity);
        } else {
            insertNewDataIntoDatabase(words);
        }
   }

    private void insertNewDataIntoDatabase(String words) {
        WordsEntity wordsEntity;
        wordsEntity = WordsEntityFactory.CreateBasicEmptyObject();
        wordsEntity.setWords(words);
        insertIntoDatabase(wordsEntity);
    }

    private void findAllWordsData() {
        lastestWordsData.setValue(getCurrentWordsInDatabase());
    }


    public MutableLiveData<List<WordsEntity>> getLastestWordsData() {
        return lastestWordsData;
    }

    public List<WordsEntity> getCurrentWordsInDatabase() {
        List<WordsEntity> currentWordData = new ArrayList<>();
        try {
            currentWordData = new DatabaseFindAllAsyncTaskHelper(wordsEntityDao).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Exception, failed calling database...");
        }
        return currentWordData;
    }

    private WordsFinderRepository() {

    }

    private void init() {
        grammarAPI = RetrofitService.createService(GrammarAPI.class);
        isInitialize = true;
    }

    private void findWordsInAPI(String type, String words, MutableLiveData<WordsResponse> wordsLiveData) {
        if (!isInitialize) {
            init();
        }

       grammarAPI.findWords(type, words).enqueue(new WordsResponseCallBack(wordsLiveData));
    }

    public void findResponseFor(String type, String words,
                                MutableLiveData<WordsResponse> wordsLiveData) {
        findWordsInAPI(type, words, wordsLiveData);
    }

    public void findWords(String words) {
        try {
            findWordsInDatabase(words);
        } catch (InterruptedException | ExecutionException e1) {
          System.out.println("Exception, failed calling database...");
        }
    }


}
