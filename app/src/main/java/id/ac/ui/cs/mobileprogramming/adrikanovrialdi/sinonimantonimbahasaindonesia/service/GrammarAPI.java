package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.service;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GrammarAPI {

    @GET("{type}/{words}")
    Call<WordsResponse> findWords(@Path("type")String wordsQuery, @Path("words") String word);
}
