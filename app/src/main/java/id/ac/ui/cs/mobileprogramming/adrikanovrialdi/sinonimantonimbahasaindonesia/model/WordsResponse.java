package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model;

import com.google.gson.annotations.SerializedName;

public class WordsResponse {
    @SerializedName("status")
    private int status;

    @SerializedName("message")
    private String message;

    @SerializedName("Result")
    private String wordResult;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getWordResult() {
        return wordResult;
    }

    public void setWordResult(String wordResult) {
        this.wordResult = wordResult;
    }
}
