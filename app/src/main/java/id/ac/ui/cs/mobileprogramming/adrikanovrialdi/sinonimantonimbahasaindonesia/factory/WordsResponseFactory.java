package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.factory;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsResponse;

public class WordsResponseFactory {

    public static WordsResponse createResponseFromString(String response) {
        WordsResponse wordsResponse = new WordsResponse();
        wordsResponse.setWordResult(response);
        wordsResponse.setStatus(200);
        wordsResponse.setMessage("Success");
        return wordsResponse;
    }

    public static WordsResponse createFailedResponse() {
        WordsResponse dataResponse = new WordsResponse();
        dataResponse.setMessage("Failed");
        dataResponse.setStatus(500);
        dataResponse.setWordResult("-");
        return dataResponse;
    }
}
