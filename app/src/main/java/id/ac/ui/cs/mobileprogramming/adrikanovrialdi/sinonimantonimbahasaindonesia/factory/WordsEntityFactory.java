package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.factory;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;

public class WordsEntityFactory {

    public static void fillEntityDataForInsert(WordsEntity wordsEntity) {
        wordsEntity.setCountSearch(1);
        wordsEntity.createOrUpdateTimestamp();
        wordsEntity.setWords(wordsEntity.getWords().toLowerCase());
    }

    public static void updateWordsEntityData(WordsEntity wordsEntity) {
        wordsEntity.setCountSearch(wordsEntity.getCountSearch()+1);
        wordsEntity.createOrUpdateTimestamp();
    }


    public static WordsEntity CreateBasicEmptyObject() {
        WordsEntity wordsEntity;
        wordsEntity = new WordsEntity();
        wordsEntity.setWords("-");
        return wordsEntity;
    }
}
