package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.view;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.R;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.adapter.WordsEntitesRecylerAdapter;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.viewmodel.RecentHistoriesViewModel;

public class RecentHistoryFragment extends Fragment {
    private RecyclerView recyclerView;
    private WordsEntitesRecylerAdapter recylerAdapter;
    private RecentHistoriesViewModel recentHistoriesViewModel;

    public static RecentHistoryFragment newInstance() {
        return new RecentHistoryFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view=  inflater.inflate(R.layout.recent_history_fragment, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        System.out.println("Starting activity...");
        super.onActivityCreated(savedInstanceState);
        recentHistoriesViewModel = ViewModelProviders.of(this).get(RecentHistoriesViewModel.class);
        prepare();
    }

    private void prepare() {
        recyclerView = getView().findViewById(R.id.recycler_view);
        recylerAdapter = new WordsEntitesRecylerAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recylerAdapter);
        recentHistoriesViewModel.getRecentWordData().observe(this, wordData -> {
            recylerAdapter.setWordsEntities(wordData);
        });
    }


}
