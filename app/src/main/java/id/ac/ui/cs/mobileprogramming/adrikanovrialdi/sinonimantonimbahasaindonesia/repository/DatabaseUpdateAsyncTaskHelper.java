package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.repository;

import android.os.AsyncTask;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.dao.WordsEntityDao;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.factory.WordsEntityFactory;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;

public class DatabaseUpdateAsyncTaskHelper extends AsyncTask<WordsEntity, Void, Void> {

    private WordsEntityDao entityDao;

    public DatabaseUpdateAsyncTaskHelper(WordsEntityDao entityDao) {
        this.entityDao = entityDao;
    }

    @Override
    protected Void doInBackground(WordsEntity... wordsEntities) {
        WordsEntity wordsEntity = wordsEntities[0];
        WordsEntityFactory.updateWordsEntityData(wordsEntity);
        entityDao.update(wordsEntity);
        return null;
    }
}
