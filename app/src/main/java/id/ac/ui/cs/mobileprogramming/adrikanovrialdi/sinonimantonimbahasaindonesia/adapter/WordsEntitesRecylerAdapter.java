package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.adapter;

import android.icu.text.SymbolTable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.R;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.repository.WordsFinderRepository;

public class WordsEntitesRecylerAdapter extends RecyclerView.Adapter<WordsEntitesRecylerAdapter.WordsEntitiesViewHolder> {
    private List<WordsEntity> wordsEntities;
    private WordsFinderRepository wordsFinderRepository = WordsFinderRepository.getInstance();

    public WordsEntitesRecylerAdapter() {
        wordsEntities = wordsFinderRepository.getCurrentWordsInDatabase();
    }

    public void setWordsEntities(List<WordsEntity> wordsEntities) {
        this.wordsEntities = wordsEntities;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WordsEntitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.card_view_holder, parent, false);
        return new WordsEntitiesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WordsEntitiesViewHolder holder, int position) {
        System.out.println("Bind view Holder");
        holder.wordsName.setText(wordsEntities.get(position).getWords());
        holder.wordsTimeStamp.setText(String.valueOf(wordsEntities.get(position).getTimestamp()));
        holder.wordsCount.setText(String.valueOf(wordsEntities.get(position).getCountSearch()));
    }

    @Override
    public int getItemCount() {
        return wordsEntities!=null ? wordsEntities.size() : 0;
    }


    static class WordsEntitiesViewHolder extends RecyclerView.ViewHolder {
        private TextView wordsName, wordsCount, wordsTimeStamp;

        WordsEntitiesViewHolder(@NonNull View itemView) {
            super(itemView);
            wordsName = itemView.findViewById(R.id.txt_word_name);
            wordsCount = itemView.findViewById(R.id.txt_count_search);
            wordsTimeStamp = itemView.findViewById(R.id.txt_time_stamp);
        }
    }
}
