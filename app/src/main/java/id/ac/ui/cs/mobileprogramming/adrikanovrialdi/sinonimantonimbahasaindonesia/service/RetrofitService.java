package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.service;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {

    static {
        System.loadLibrary("native-lib");
    }

    public static native String getAPIUrl();

    private static OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build();

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(getAPIUrl())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();


    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
