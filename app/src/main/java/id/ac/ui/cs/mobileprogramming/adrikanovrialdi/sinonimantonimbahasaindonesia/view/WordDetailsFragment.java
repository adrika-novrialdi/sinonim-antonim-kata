package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.view;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.R;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsResponse;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.viewmodel.SynonymAntonymViewModel;

public class WordDetailsFragment extends Fragment {

    private SynonymAntonymViewModel synonymAntonymViewModel;
    private TextView synonymWordResult;
    private TextView antonymWordResult;
    private Button openGlButton;

    public static WordDetailsFragment newInstance() {
        return new WordDetailsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.word_details_fragment, container, false);
        openGlButton = view.findViewById(R.id.openGlButton);
        synonymWordResult = view.findViewById(R.id.SynonymResultTextView);
        antonymWordResult = view.findViewById(R.id.AntonymResultTextView);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)  {
        super.onActivityCreated(savedInstanceState);
        synonymAntonymViewModel = ViewModelProviders.of(this).get(SynonymAntonymViewModel.class);
        synonymAntonymViewModel.setApplicationContext(getContext());
        updateSynonymData();
        updateAntonymData();
        setUpButtonListener();
    }



    private void updateSynonymData() {
        synonymAntonymViewModel.getCurrentWordsSynonym().observe(this, new Observer<WordsResponse>() {
            @Override
            public void onChanged(WordsResponse wordsResponse) {
                System.out.println("CHANGED sinonim");
                System.out.println(wordsResponse.getWordResult());
                synonymWordResult.setText(wordsResponse.getWordResult());
            }
        });
    }

    private void updateAntonymData() {
        synonymAntonymViewModel.getCurrentWordsAntonym().observe(this, antonym -> {
            //System.out.println("CALLED");
            System.out.println("CHANGED antonum");
            antonymWordResult.setText(antonym.getWordResult());
            System.out.println(antonym.getWordResult());
        });
    }

    private void setUpButtonListener() {
        openGlButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), GLES20Activity.class));
            }
        });
    }


}
