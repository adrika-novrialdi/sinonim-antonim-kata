package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.repository;

import android.os.AsyncTask;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.dao.WordsEntityDao;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;

public class DatabaseFindAllAsyncTaskHelper extends AsyncTask<Void, Void, List<WordsEntity>>  {

    private WordsEntityDao wordsEntityDao;

    public DatabaseFindAllAsyncTaskHelper(WordsEntityDao wordsEntityDao) {
        this.wordsEntityDao = wordsEntityDao;
    }

    @Override
    protected List<WordsEntity> doInBackground(Void... voids) {
        return wordsEntityDao.findAll();
    }
}
