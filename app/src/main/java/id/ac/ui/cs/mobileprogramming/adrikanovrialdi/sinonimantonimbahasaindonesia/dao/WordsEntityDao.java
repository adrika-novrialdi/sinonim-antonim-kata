package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
import java.util.Optional;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;

@Dao
public interface WordsEntityDao {
    @Insert
    void insert(WordsEntity wordsEntity);

    @Update
    void update(WordsEntity wordsEntity);

    @Query("SELECT * FROM words_entity ORDER BY timestamp DESC LIMIT 10")
    List<WordsEntity> findAll();

    @Query("SELECT * FROM words_entity WHERE words= :wordsName")
    WordsEntity findWordsByWordsName(String wordsName);
}
