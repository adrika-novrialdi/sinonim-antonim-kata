package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.view;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.R;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.viewmodel.SynonymAntonymViewModel;

public class WordsFinderFragment extends Fragment {

    private SynonymAntonymViewModel synonymAntonymViewModel;
    private Button searchButton;
    private EditText wordToSearch;


    public static WordsFinderFragment newInstance() {
        return new WordsFinderFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.words_finder_fragment, container, false);
        searchButton = view.findViewById(R.id.searchButton);
        wordToSearch = view.findViewById(R.id.wordToSearch);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        synonymAntonymViewModel = ViewModelProviders.of(this).get(SynonymAntonymViewModel.class);
        synonymAntonymViewModel.setApplicationContext(getContext());
        setUpButtonListener();
    }

    private String getWordsToSearch() {
        String currentTextViewData = wordToSearch.getText().toString();
        if (!currentTextViewData.isEmpty()) {
            return wordToSearch.getText().toString();
        } else {
            return "-";
        }
    }

    private void setCurrentWordsToView(String words) {
        synonymAntonymViewModel.findWords(words);
    }

    private boolean isConnectedToNetwork() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo!=null && networkInfo.isConnected() ) {
            return true;
        }

        return false;
    }

    private void notifyNoInternet() {
        Toast.makeText(getContext(), "Please connect to Internet", Toast.LENGTH_SHORT).show();
    }

    private void setUpButtonListener() {
        searchButton.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (isConnectedToNetwork()) {
                    setCurrentWordsToView(getWordsToSearch());
                } else {
                    notifyNoInternet();
                }
            }
        });
    }

}
