package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.util.Timer;
import java.util.TimerTask;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.R;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.view.MainActivity;

public class NotificationService extends Service {
    private Timer mTimer;
    NotificationManager notificationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            if (!Utils.isConnectedToNetwork(getApplicationContext())) {
                sendNotification();
            }

        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mTimer = new Timer();
        mTimer.schedule(timerTask, 2000, 2*1000);
    }

    private void sendNotification() {
        String channelID = "id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia";
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannel(
                "id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia",
                "NotifyInternet",
                "Internet Notifiation Channel");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("NotificationService");

        Intent notificationIntent = new Intent(this, MainActivity.class );
        notificationIntent.addCategory(Intent. CATEGORY_LAUNCHER ) ;
        notificationIntent.setAction(Intent. ACTION_MAIN ) ;
        notificationIntent.setFlags(Intent. FLAG_ACTIVITY_CLEAR_TOP | Intent. FLAG_ACTIVITY_SINGLE_TOP );
        PendingIntent resultIntent = PendingIntent. getActivity (this, 0 , notificationIntent , 0 ) ;

        Context context = getApplicationContext();

        Notification.Builder builder;

        builder = new Notification.Builder(this, channelID)
                .setContentTitle("You are Disconnected")
                .setContentText("Please Connect to Internet")
                .setContentIntent(resultIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.app_icon_round);

        Notification notification = builder.build();

        notificationManager.notify(1, notification);

    }

    protected void createNotificationChannel(String id, String name,
                                             String description) {

        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel channel =
                new NotificationChannel(id, name, importance);

        channel.setDescription(description);
        channel.enableLights(true);
        channel.setLightColor(Color.RED);
        channel.enableVibration(true);
        channel.setVibrationPattern(
                new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        notificationManager.createNotificationChannel(channel);
    }

    @Override
    public void onDestroy() {
        try{
            mTimer.cancel();
            timerTask.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
        Intent intent = new Intent("id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia");
        intent.putExtra("yourvalue","torestore");
        sendBroadcast(intent);
    }
}
