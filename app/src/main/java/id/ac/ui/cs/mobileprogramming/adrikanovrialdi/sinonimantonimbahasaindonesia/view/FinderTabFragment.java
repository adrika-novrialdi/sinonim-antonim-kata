package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.view;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.R;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.viewmodel.SynonymAntonymViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class FinderTabFragment extends Fragment {

    public FinderTabFragment() {
        // Required empty public constructor
    }

    public static FinderTabFragment newInstance() {
        return new FinderTabFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_finder_tab, container, false);
        synonymAntonymViewModel =  ViewModelProviders.of(this).get(SynonymAntonymViewModel.class);
        synonymAntonymViewModel.setApplicationContext(getContext());
        observerOnViewModelStatusChanged();
        return view;
    }

    private SynonymAntonymViewModel synonymAntonymViewModel;



    private void observerOnViewModelStatusChanged() {
        synonymAntonymViewModel.getStatus().observe(this, data -> {
            if (data) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                Fragment detailsFragment = WordDetailsFragment.newInstance();
                transaction.add(R.id.wordDetailsFragment, detailsFragment);
                transaction.commit();
            }
        });
    }
}
