package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.repository.WordsFinderRepository;

public class RecentHistoriesViewModel extends ViewModel {

    private WordsFinderRepository repository = WordsFinderRepository.getInstance();
    private static MutableLiveData<List<WordsEntity>> recentWordData ;

    public RecentHistoriesViewModel() {
    }

    public MutableLiveData<List<WordsEntity>> getRecentWordData() {
        if (recentWordData==null) {
            recentWordData = repository.getLastestWordsData();
        }
        return recentWordData;
    }
}
