package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.dao.WordsEntityDao;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;

@Database(entities = {WordsEntity.class}, version = 1)
public abstract class WordEntityDatabase extends RoomDatabase {
    private static WordEntityDatabase ourInstance;

    public static WordEntityDatabase getDatabase(final Context context) {
        if (ourInstance == null) {
            synchronized (WordEntityDatabase.class) {
                if (ourInstance == null) {
                    ourInstance = Room.databaseBuilder(context.getApplicationContext(),
                            WordEntityDatabase.class, "words_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return ourInstance;
    }

    public abstract WordsEntityDao getWordsEntityDao();
}
