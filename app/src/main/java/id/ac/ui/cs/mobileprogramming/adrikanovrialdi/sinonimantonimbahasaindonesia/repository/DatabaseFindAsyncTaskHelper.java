package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.repository;

import android.os.AsyncTask;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.dao.WordsEntityDao;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;

public class DatabaseFindAsyncTaskHelper extends AsyncTask<WordsEntity, Void, WordsEntity> {
    private WordsEntityDao entityDao;
    private String wordsToSearch;

    public DatabaseFindAsyncTaskHelper(WordsEntityDao entityDao, String wordsToSearch) {
        this.entityDao = entityDao;
        this.wordsToSearch = wordsToSearch.toLowerCase();
    }

    @Override
    protected WordsEntity doInBackground(WordsEntity... wordsEntities) {
        WordsEntity wordsEntityFromDb = entityDao.findWordsByWordsName(wordsToSearch);
        if (wordsEntityFromDb != null) {
            System.out.println("Found.. " + wordsEntityFromDb.getWords());
        }
        return wordsEntityFromDb;
    }
}
