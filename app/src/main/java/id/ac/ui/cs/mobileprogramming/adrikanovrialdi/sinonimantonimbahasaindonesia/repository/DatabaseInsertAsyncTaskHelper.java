package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.repository;

import android.os.AsyncTask;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.dao.WordsEntityDao;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.factory.WordsEntityFactory;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;

class DatabaseInsertAsyncTaskHelper extends AsyncTask<WordsEntity, Void, Void> {
     private WordsEntityDao wordsEntityDao;

     DatabaseInsertAsyncTaskHelper(WordsEntityDao wordsEntityDao) {
        this.wordsEntityDao = wordsEntityDao;
    }

    @Override
    protected Void doInBackground(WordsEntity... wordsEntities) {
         WordsEntity wordsEntity = wordsEntities[0];
         WordsEntityFactory.fillEntityDataForInsert(wordsEntity);
         System.out.println("Trying to insert...");
         wordsEntityDao.insert(wordsEntity);
         System.out.println(wordsEntity.getWords() + " is saved successfully");
         return null;
    }
}
