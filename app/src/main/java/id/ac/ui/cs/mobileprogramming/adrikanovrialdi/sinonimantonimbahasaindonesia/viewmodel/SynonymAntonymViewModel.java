package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsEntity;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsResponse;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.repository.WordsFinderRepository;

public class SynonymAntonymViewModel extends ViewModel {
    private static MutableLiveData<WordsResponse> wordResponseSynonym = new MutableLiveData<>();
    private static MutableLiveData<WordsResponse> wordsResponseAntonym = new MutableLiveData<>();
    private WordsFinderRepository wordsFinderRepository = WordsFinderRepository.getInstance();
    private static MutableLiveData<Boolean> isCalled = new MutableLiveData<>();
    private static WordsEntity wordsEntity;

    public void findWords(String words)  {
        wordsFinderRepository.findWords(words);
        findSynonym(words);
        findAntonym(words);
        isCalled.setValue(true);
    }

    public SynonymAntonymViewModel() {

    }


    public void setApplicationContext(Context context) {
        wordsFinderRepository.setApplicationContext(context);
    }


    private void findSynonym(String words)  {
        wordsFinderRepository.findResponseFor("sinonim", words, wordResponseSynonym);

    }

    private void findAntonym(String words)  {
        wordsFinderRepository.findResponseFor("antonim", words, wordsResponseAntonym);
    }

    public MutableLiveData<WordsResponse> getCurrentWordsSynonym() {

        return wordResponseSynonym;
    }

    public MutableLiveData<WordsResponse> getCurrentWordsAntonym() {

        return wordsResponseAntonym;
    }

    public MutableLiveData<Boolean> getStatus() {
        return isCalled;
    }

}
