package id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.service;

import androidx.lifecycle.MutableLiveData;

import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.factory.WordsResponseFactory;
import id.ac.ui.cs.mobileprogramming.adrikanovrialdi.sinonimantonimbahasaindonesia.model.WordsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WordsResponseCallBack implements Callback<WordsResponse> {

    private MutableLiveData<WordsResponse> wordsResponseMutableLiveData;

    public WordsResponseCallBack(MutableLiveData<WordsResponse> wordsResponseMutableLiveData) {
        this.wordsResponseMutableLiveData = wordsResponseMutableLiveData;
    }

    @Override
    public void onResponse(Call<WordsResponse> call, Response<WordsResponse> response) {
        if (response.body().getWordResult()!=null) {
            wordsResponseMutableLiveData.setValue(response.body());
        } else {
            wordsResponseMutableLiveData.setValue(WordsResponseFactory.createFailedResponse());
        }
    }

    @Override
    public void onFailure(Call<WordsResponse> call, Throwable t) {
        System.out.println("FAILED");
        wordsResponseMutableLiveData.setValue(WordsResponseFactory.createFailedResponse());

    }


}
